Source: libcupsfilters
Maintainer: Debian Printing Team <debian-printing@lists.debian.org>
Uploaders:
 Till Kamppeter <till.kamppeter@gmail.com>,
 Thorsten Alteholz <debian@alteholz.de>,
Section: net
Priority: optional
Build-Depends:
 autoconf,
 debhelper-compat (= 13),
 dh-sequence-pkgkde-symbolshelper,
 fonts-dejavu-core,
 ghostscript,
 libavahi-client-dev,
 libavahi-common-dev,
 libcups2-dev (>= 2.2.2),
 libcupsimage2-dev,
 libdbus-1-dev,
 libfontconfig-dev,
 libfreetype-dev,
 libglib2.0-dev,
 libjpeg-dev,
 liblcms2-dev,
 libpng-dev,
 libpoppler-cpp-dev,
 libqpdf-dev (>= 8.3~),
 librsvg2-bin,
 libtiff-dev,
 pkg-config,
 pkg-kde-tools,
 poppler-utils,
 zlib1g-dev,
 libexif-dev,
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/printing-team/libcupsfilters
Vcs-Git: https://salsa.debian.org/printing-team/libcupsfilters.git
Homepage: http://www.openprinting.org/
Rules-Requires-Root: no

Package: libcupsfilters2
Architecture: any
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 libcupsfilters2-common (>= ${source:Version})
Pre-Depends:
 ${misc:Pre-Depends},
Description: OpenPrinting libcupsfilters - Shared library
 This library contains filter functions doing all kinds of file format
 conversion used for printing and scanning, to be used in Printer
 Applications, CUPS filters, printer drivers, ... also other commonly
 used functions for handling print data, like color space/depth
 conversion, dithering, IPP message parsing, ... are available.

Package: libcupsfilters2-common
Architecture: all
Section: libs
Depends: ${misc:Depends}
Recommends: libcupsfilters2 (>= ${source:Version})
Breaks: cups-filters (<< 2.0~)
Replaces: cups-filters (<< 2.0~)
Description: OpenPrinting libcupsfilters - Auxiliary files
 This package contains system-architecture-independent auxiliary files
 for libcupsfilters. These are character sets for the
 cfFilterTextToPDF() filter function and banner/test page instruction
 files and templates for the cfFilterBannerToPDF() filter function.

Package: libcupsfilters2-dev
Architecture: any
Multi-Arch: same
Section: libdevel
# add Conflicts: for now, maybe later the libs should be renamed
Conflicts: libcupsfilters-dev
Depends:
 libcupsfilters2 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: OpenPrinting libcupsfilters - Development files for the library
 This library contains filter functions doing all kinds of file format
 conversion used for printing and scanning, to be used in Printer
 Applications, CUPS filters, printer drivers, ... also other commonly
 used functions for handling print data, like color space/depth
 conversion, dithering, IPP message parsing, ... are available.
 .
 This package contains the header files to develop applications (Printer
 Applications, CUPS filters, printer drivers) using libcupsfilters.

Package: libcupsfilters-tests
Architecture: any
Multi-Arch: foreign
Section: utils
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 fonts-dejavu-core,
Description: OpenPrinting libcupsfilters - Test programs for autopkgtests
 This library contains filter functions doing all kinds of file format
 conversion used for printing and scanning, to be used in Printer
 Applications, CUPS filters, printer drivers, ... also other commonly
 used functions for handling print data, like color space/depth
 conversion, dithering, IPP message parsing, ... are available.
 .
 This package contains test programs from the upstream code which are used
 for the autopkgtests.
 .
 For using libcupsfilters you do not need to install this package. It does not
 contain anything useful for end users.
